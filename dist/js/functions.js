$(document).ready(function(){
	
  $("a").on('click', function(event) {
    // Smooth scrolling on button
    if (this.hash !== "") {
      event.preventDefault();

      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
           window.location.hash = hash;
      });
    } 
  });

  // Countdown javascript
  var countDownDate = new Date("Mar 1, 2018 15:37:25").getTime();

  // Update the count down every 1 second
  var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Display the result in the element with id="demo"
    document.getElementById("countdown").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";

    // If the count down is finished, write some text 
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("countdown").innerHTML = "0d 0h 0m 0s";
    }
  }, 1000);

  //Download ICS file to add event to calendar
  cal = ics();
  cal.addEvent('Webcast: Introduction to Using GitLab', 'A webcast introducing the benefits of using GitLab as a collaboration tool.', 'Webcast', '03/01/2018 12:00 pm', '03/01/2018 1:00 pm');
  
  if($(window).width()>600){
    $(".datetime").on('click', function() {
      cal.download('Webcast: Introduction to Using GitLab')
    });
  }

});