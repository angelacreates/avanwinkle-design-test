var gulp          = require('gulp');
var sass          = require('gulp-sass');
var del           = require('del');
var browserSync   = require('browser-sync').create();
var reload        = browserSync.reload();
var autoprefix    = require('gulp-autoprefixer');
var jshint        = require('gulp-jshint');
var pug           = require('gulp-pug');






var _pug_src      = './src/**/!(_)*.pug';
var _pug_dest     = './dist/';
var _pug_watch    = './src/**/*.pug';
var _scss_src     = './src/scss/styles.scss';
var _scss_dest    = './dist/css';
var _scss_watch   = './src/scss/**/*.scss';
var _js_src       = './src/js/**/*.js';
var _js_dest      = './dist/js/';



gulp.task('browser-sync', function() {
    browserSync.init({
      reloadDelay: 1000,
       server: {
          baseDir: './dist'
       }
    });
});

gulp.task('del', function() {
   del(['./dist/']);
});

gulp.task('sass', function() {
  gulp.src(_scss_src)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefix('last 2 versions'))
    .pipe(gulp.dest(_scss_dest))
    .pipe(browserSync.stream());
});

gulp.task('pug', function() {
  gulp.src(_pug_src)
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest(_pug_dest))
    .pipe(browserSync.stream());
});



gulp.task('images', function() {
  gulp.src('./src/images/**.*')
     .pipe(gulp.dest('./dist/images'))
     .pipe(browserSync.stream());
});

gulp.task('js', function() {
  gulp.src(_js_src)
     .pipe(jshint())
     .pipe(gulp.dest(_js_dest))
     .pipe(browserSync.reload({
      stream: true
    }));
});


gulp.task('watch', function() {
    gulp.watch(_scss_watch , ['sass'], browserSync.reload);
    gulp.watch(_pug_watch , ['pug'], browserSync.reload);
    gulp.watch(_js_src     , ['js'],   browserSync.reload);
    gulp.watch('./dist/images'     , ['images'],   browserSync.reload);
});

gulp.task('default', [ 'images', 'sass' , 'pug' , 'js' , 'watch' , 'browser-sync']);
